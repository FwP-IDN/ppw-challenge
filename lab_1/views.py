from django.shortcuts import render
from datetime import datetime, date
# Enter your name here
mhs_name = 'Febriananda Wida Pramudita'  # TODO Implement this
curr_year = int(datetime.now().strftime("%Y"))
birth_date = date(1999, 2, 16)  # TODO Implement this, format (Year, Month, Date)
NPM = 1706028631  # TODO Implement this
# Create your views here.


def index(request):
    return render(request, 'index_homepage.html')


def indexDepan(request):
    response = {'name': 'White Board', 'age': calculate_age(date(1993, 9, 12).year), 'npm': '-',
                'university': 'Universitas Indonesia', 'hobby': 'Silent',
                'my_desc': 'I am a silent white board',
                'photo_link':
                'https://lh3.googleusercontent.com/5YZR1EFWjYSY1aKWfW0kzyIGFgD6DOUws6nypStXIJAH3eN'
                '7Ym5B76XewEGC5y2fSiEkuzuqREohlUdYEZfR=w1280-h655-rw'}
    return render(request, 'index_lab1.html', response)


def indexBelakang(request):
    response = {'name': 'Vincentius Adi Kurnianto', 'age': calculate_age(date(1999, 1, 31).year), 'npm': 1706979480,
                'university': 'Universitas Indonesia', 'hobby': 'Memancing Ikan',
                'my_desc': 'I am a "trap" person',
                'photo_link':
                'https://scontent.fcgk4-2.fna.fbcdn.net/v/t1.0-9/32661353_1782651678463862_4003671'
                '477135081472_n.jpg?_nc_cat=0&oh=dc9ba8e9873de7452390675a32e52e87&oe=5BF340F9'}
    return render(request, 'index_lab1.html', response)


def calculate_age(birth_year):
    return curr_year - birth_year if birth_year <= curr_year else 0
