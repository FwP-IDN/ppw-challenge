from django.urls import re_path
from .views import indexDepan, indexBelakang, index
# url for app
urlpatterns = [
    re_path(r'^$', index, name='index'),
    re_path(r'^depan$', indexDepan, name='indexDepan'),
    re_path(r'^belakang$', indexBelakang, name='indexBelakang'),
]
